package config

import (
	"fmt"
	"os"

	"github.com/google/logger"

	"github.com/BurntSushi/toml"
)

// Config contains all configuration sections for application
type Config struct {
	Web      web
	Database database
}

type web struct {
	Host string
}

type database struct {
	Driver string
	DSN    string
}

// Load parses the given toml file and returns the Config object
func Load(tomlFile string) Config {
	if _, err := os.Stat(tomlFile); os.IsNotExist(err) {
		logger.Errorln("Unable to locate \"config.toml\" in execution directory")
		fmt.Println("Unable to locate \"config.toml\" in execution directory")
		os.Exit(1)
	}

	var conf Config
	if _, err := toml.DecodeFile(tomlFile, &conf); err != nil {
		logger.Errorf("Unable to parse config.toml: %v\n", err)
		os.Exit(1)
	}

	return conf
}
