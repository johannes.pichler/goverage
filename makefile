PLATFORMS := linux/amd64 darwin/amd64 windows/amd64

temp = $(subst /, , $@)
os = $(word 1, $(temp))
arch = $(word 2, $(temp))

all: build

build: clean frontend $(PLATFORMS)

clean:
	-rm -rf html-files
	-rm rice-box.go
	mkdir html-files

frontend:
	cd frontend && npm run build && cp -R dist/* ../html-files
	~/go/bin/rice embed-go

$(PLATFORMS):
	GOOS=$(os) GOARCH=$(arch) go build -o 'build/goverage.$(os).$(arch)' main.go rice-box.go

run:
	go run main.go

.PHONY: frontend