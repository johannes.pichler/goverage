package storage

import (
	"time"

	"github.com/fetzi/goverage/models"
	"github.com/google/logger"

	_ "github.com/go-sql-driver/mysql" // mysql dialect
	"github.com/jinzhu/gorm"
	"github.com/jinzhu/now"
)

const dailyCoverageToCalculate = 30

// Storage contains the database instance
type Storage struct {
	db *gorm.DB
}

type coverageGroup struct {
	ProjectID           uint
	Date                time.Time
	Coverage            float64
	ConditionalCoverage float64
	MethodCoverage      float64
	ElementCoverage     float64
	LinesOfCode         float64
}

type testsuiteGroup struct {
	ProjectID uint
	Date      time.Time
	Errors    float64
	Failures  float64
	Skipped   float64
	Time      float64
	Tests     float64
}

// Connect establishes the database connection and migrates the database schema
func Connect(driver string, dsn string) (Storage, error) {
	db, err := gorm.Open(driver, dsn)

	if err != nil {
		logger.Fatalln(err)
		panic(err)
	}

	db.AutoMigrate(&models.Project{}, &models.Coverage{}, &models.CoverageDaily{}, &models.Testsuite{}, &models.TestsuiteDaily{})
	return Storage{db}, err
}

// GetProject retrieves an existing project or creates a new one
func (storage Storage) GetProject(name string) *models.Project {
	var project models.Project

	storage.db.FirstOrCreate(&project, models.Project{Name: name})

	return &project
}

// GetProjects retrieves a list of projects
func (storage Storage) GetProjects() []*models.Project {
	projects := []*models.Project{}
	storage.db.Find(&projects)

	return projects
}

// StoreCoverage stores the passed coverage object in the database
func (storage Storage) StoreCoverage(project *models.Project, coverage *models.Coverage) {
	coverage.ProjectID = project.ID

	storage.db.Create(coverage)
}

// StoreTestsuite stores the passed testsuite object in the database
func (storage Storage) StoreTestsuite(project *models.Project, testsuite *models.Testsuite) {
	testsuite.ProjectID = project.ID

	storage.db.Create(testsuite)
}

// GetCoverage retrieves coverage data for a given project
func (storage Storage) GetCoverage(project *models.Project, amount int) []*models.Coverage {
	coverageEntries := []*models.Coverage{}

	storage.db.Where("project_id = ?", project.ID).Order("created_at desc").Limit(amount).Find(&coverageEntries)

	return coverageEntries
}

// GetDailyCoverage retrieves daily coverage data for a project
func (storage Storage) GetDailyCoverage(project *models.Project, amount int) []*models.CoverageDaily {
	coverageEntries := []*models.CoverageDaily{}

	storage.db.Where("project_id = ?", project.ID).Order("date desc").Limit(amount).Find(&coverageEntries)

	return coverageEntries
}

// GetTestsuites retrieves testsuite data for a project
func (storage Storage) GetTestsuites(project *models.Project, amount int) []*models.Testsuite {
	testsuiteEntries := []*models.Testsuite{}

	storage.db.Where("project_id = ?", project.ID).Order("created_at desc").Limit(amount).Find(&testsuiteEntries)

	return testsuiteEntries
}

// GetDailyTestsuites retrieves daily testsuite data for a project
func (storage Storage) GetDailyTestsuites(project *models.Project, amount int) []*models.TestsuiteDaily {
	testsuiteEntries := []*models.TestsuiteDaily{}

	storage.db.Where("project_id = ?", project.ID).Order("created_at desc").Limit(amount).Find(&testsuiteEntries)

	return testsuiteEntries
}

// CalculateDailyAggregates generates daily aggregations for coverage data
func (storage Storage) CalculateDailyAggregates() {
	logger.Infoln("starting daily aggregates calculation")
	date := time.Now()
	start := now.New(date).Add(time.Hour * -24 * dailyCoverageToCalculate)
	end := now.New(date).EndOfDay()

	results := []coverageGroup{}

	storage.db.Table("coverages").Select("project_id, date(created_at) as date, avg(total_coverage) as coverage, avg(conditional_coverage) as conditional_coverage, avg(method_coverage) as method_coverage, avg(element_coverage) as element_coverage, avg(lines_of_code) as lines_of_code").Where("created_at between ? and ?", start, end).Group("project_id, date(created_at)").Scan(&results)

	for _, coverageGroup := range results {
		dailyCoverage := models.CoverageDaily{}

		storage.db.FirstOrCreate(&dailyCoverage, models.CoverageDaily{ProjectID: coverageGroup.ProjectID, Date: coverageGroup.Date})
		storage.db.Model(&dailyCoverage).Updates(map[string]interface{}{
			"coverage":             coverageGroup.Coverage,
			"conditional_coverage": coverageGroup.ConditionalCoverage,
			"method_coverage":      coverageGroup.MethodCoverage,
			"element_coverage":     coverageGroup.ElementCoverage,
			"lines_of_code":        coverageGroup.LinesOfCode,
		})
	}

	testsuiteResults := []testsuiteGroup{}

	storage.db.Table("testsuites").Select("project_id, date(created_at) as date, avg(errors) as errors, avg(failures) as failures, avg(skipped) as skipped, avg(time) as time, avg(tests) as tests").Where("created_at between ? and ?", start, end).Group("project_id, date(created_at)").Scan(&testsuiteResults)

	for _, testsuiteGroup := range testsuiteResults {
		dailyTestsuite := models.TestsuiteDaily{}

		storage.db.FirstOrCreate(&dailyTestsuite, models.TestsuiteDaily{ProjectID: testsuiteGroup.ProjectID, Date: testsuiteGroup.Date})
		storage.db.Model(&dailyTestsuite).Updates(map[string]interface{}{
			"errors":   testsuiteGroup.Errors,
			"failures": testsuiteGroup.Failures,
			"skipped":  testsuiteGroup.Skipped,
			"time":     testsuiteGroup.Time,
			"tests":    testsuiteGroup.Tests,
		})
	}

	logger.Infoln("finished daily aggregates calculation")
}
