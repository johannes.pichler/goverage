Goverage is a web application for collecting coverage and testsuite data for various projects.

## Configuration
Beside the goverage executable a `config.toml` file must be created.

```toml
[web]
host = "127.0.0.1:1515"

[database]
driver = "mysql"
dsn = "gouser:gouser@tcp(127.0.0.1:3306)/goverage?charset=utf8&parseTime=True&loc=Local"
```

This file is responsible for defining the web-port binding and the dns for the database connection.