package parser

import (
	"encoding/xml"
	"io"

	"github.com/fetzi/goverage/models"
)

type testsuites struct {
	XMLName xml.Name    `xml:"testsuites"`
	Childs  []testsuite `xml:"testsuite"`
}

type testsuite struct {
	XMLName  xml.Name `xml:"testsuite"`
	Errors   int      `xml:"errors,attr"`
	Failures int      `xml:"failures,attr"`
	Skipped  int      `xml:"skipped,attr"`
	Time     float64  `xml:"time,attr"`
	Tests    int      `xml:"tests,attr"`
}

func ParseTestsuites(reader io.Reader) (*models.Testsuite, error) {
	var testsuites testsuites
	if err := xml.NewDecoder(reader).Decode(&testsuites); err != nil {
		return nil, err
	}

	errors := 0
	failures := 0
	skipped := 0
	time := 0.0
	tests := 0

	for _, testsuite := range testsuites.Childs {
		errors += testsuite.Errors
		failures += testsuite.Failures
		skipped += testsuite.Skipped
		time += testsuite.Time
		tests += testsuite.Tests
	}

	testsuite := models.Testsuite{
		Errors:   uint(errors),
		Failures: uint(failures),
		Skipped:  uint(skipped),
		Time:     time,
		Tests:    uint(tests),
	}

	return &testsuite, nil
}
