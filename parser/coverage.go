package parser

import (
	"encoding/xml"
	"io"

	"github.com/fetzi/goverage/models"
)

type xmlCoverage struct {
	XMLName xml.Name `xml:"coverage"`
	Project project  `xml:"project"`
}

type project struct {
	XMLName xml.Name `xml:"project"`
	Metrics metric   `xml:"metrics"`
}

type metric struct {
	XMLName             xml.Name `xml:"metrics"`
	Statements          int      `xml:"statements,attr"`
	CoveredStatements   int      `xml:"coveredstatements,attr"`
	Conditionals        int      `xml:"conditionals,attr"`
	CoveredConditionals int      `xml:"coveredconditionals,attr"`
	Methods             int      `xml:"methods,attr"`
	CoveredMethods      int      `xml:"coveredmethods,attr"`
	Elements            int      `xml:"elements,attr"`
	CoveredElements     int      `xml:"coveredelements,attr"`
	LinesOfCode         int      `xml:"loc,attr"`
}

// ParseCoverage parses the given coverage file and exracts the key metrics
func ParseCoverage(reader io.Reader) (*models.Coverage, error) {
	var xmlCoverage xmlCoverage
	if err := xml.NewDecoder(reader).Decode(&xmlCoverage); err != nil {
		return nil, err
	}

	statements := xmlCoverage.Project.Metrics.Statements
	coveredStatements := xmlCoverage.Project.Metrics.CoveredStatements
	conditionals := xmlCoverage.Project.Metrics.Conditionals
	coveredConditionals := xmlCoverage.Project.Metrics.CoveredConditionals
	methods := xmlCoverage.Project.Metrics.Methods
	coveredMethods := xmlCoverage.Project.Metrics.CoveredMethods
	elements := xmlCoverage.Project.Metrics.Elements
	coveredElements := xmlCoverage.Project.Metrics.CoveredElements
	linesOfCode := xmlCoverage.Project.Metrics.LinesOfCode

	coverage := models.Coverage{
		Statements:          statements,
		CoveredStatements:   coveredStatements,
		TotalCoverage:       float64(coveredStatements) / (float64(statements) / 100),
		Conditionals:        conditionals,
		CoveredConditionals: coveredConditionals,
		ConditionalCoverage: float64(coveredConditionals) / (float64(conditionals) / 100),
		Methods:             methods,
		CoveredMethods:      coveredMethods,
		MethodCoverage:      float64(coveredMethods) / (float64(methods) / 100),
		Elements:            elements,
		CoveredElements:     coveredElements,
		ElementCoverage:     float64(coveredElements) / (float64(elements) / 100),
		LinesOfCode:         linesOfCode,
	}

	return &coverage, nil
}
