package worker

import (
	"time"

	"github.com/fetzi/goverage/storage"
)

// Aggregate triggers aggregate calculation every minute
func Aggregate(storage storage.Storage, interval int) {
	tickChan := time.NewTicker(time.Minute * time.Duration(interval)).C

	go func() {
		storage.CalculateDailyAggregates()
		for {
			select {
			case <-tickChan:
				storage.CalculateDailyAggregates()
			}
		}
	}()
}
