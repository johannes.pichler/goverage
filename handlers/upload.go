package handlers

import (
	"net/http"

	"github.com/google/logger"

	"github.com/fetzi/goverage/parser"
)

// UploadCoverageHandler implements coverage file upload
func (ctx AppContext) UploadCoverageHandler(w http.ResponseWriter, r *http.Request) {
	projectName := r.URL.Query().Get("project")
	project := ctx.Storage.GetProject(projectName)

	coverage, err := parser.ParseCoverage(r.Body)

	if err != nil {
		logger.Errorf("unable to process coverage data for %s", projectName)
		fail(w, http.StatusBadRequest, "Unable to parse coverage data")
		return
	}

	logger.Infof("uploaded coverage data for %s", projectName)

	ctx.Storage.StoreCoverage(project, coverage)

	respond(w, http.StatusOK, coverage)
}

// UploadTestHandler implements junit.xml file upload
func (ctx AppContext) UploadTestHandler(w http.ResponseWriter, r *http.Request) {
	projectName := r.URL.Query().Get("project")
	project := ctx.Storage.GetProject(projectName)

	testsuite, err := parser.ParseTestsuites(r.Body)

	if err != nil {
		logger.Errorf("unable to process testsuites data for %s", projectName)
		fail(w, http.StatusBadRequest, "Unable to parse junit.xml data")
		return
	}

	logger.Infof("uploaded testsuites data for %s", projectName)

	ctx.Storage.StoreTestsuite(project, testsuite)

	respond(w, http.StatusOK, testsuite)
}
