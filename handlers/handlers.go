package handlers

import (
	"net/http"

	"github.com/fetzi/goverage/config"
	"github.com/fetzi/goverage/storage"
	"github.com/google/jsonapi"
)

type jsonError struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

// AppContext contains all relevant context information
type AppContext struct {
	Storage storage.Storage
	Config  config.Config
}

func fail(w http.ResponseWriter, status int, message string) {
	respond(w, status, jsonError{status, message})
}

func respond(w http.ResponseWriter, status int, data interface{}) {
	w.Header().Set("Content-Type", jsonapi.MediaType)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "X-Requested-With")
	w.Header().Set("X-Requested-With", "XMLHttpRequest")
	w.WriteHeader(http.StatusOK)

	jsonapi.MarshalPayload(w, data)
}
