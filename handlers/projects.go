package handlers

import (
	"net/http"

	"github.com/gorilla/mux"
)

// GetProjectsHandler implements get project endpoint
func (ctx AppContext) GetProjectsHandler(w http.ResponseWriter, r *http.Request) {
	projects := ctx.Storage.GetProjects()

	respond(w, http.StatusOK, projects)
}

// GetProjectHandler get single project endpoint
func (ctx AppContext) GetProjectHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectName := vars["project"]
	project := ctx.Storage.GetProject(projectName)

	respond(w, http.StatusOK, project)
}

// GetCoverageHandler implements get coverage for project endpoind
func (ctx AppContext) GetCoverageHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectName := vars["project"]

	project := ctx.Storage.GetProject(projectName)
	coverage := ctx.Storage.GetCoverage(project, 30)

	respond(w, http.StatusOK, coverage)
}

// GetDailyCoverageHandler implements get daily coverage for a project
func (ctx AppContext) GetDailyCoverageHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectName := vars["project"]

	project := ctx.Storage.GetProject(projectName)
	coverage := ctx.Storage.GetDailyCoverage(project, 30)

	respond(w, http.StatusOK, coverage)
}

// GetTestsuiteHandler implements get testsuites for project endpoind
func (ctx AppContext) GetTestsuiteHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectName := vars["project"]

	project := ctx.Storage.GetProject(projectName)
	testsuites := ctx.Storage.GetTestsuites(project, 30)

	respond(w, http.StatusOK, testsuites)
}

// GetDailyTestsuiteHandler implements get testsuites for project endpoind
func (ctx AppContext) GetDailyTestsuiteHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectName := vars["project"]

	project := ctx.Storage.GetProject(projectName)
	testsuites := ctx.Storage.GetDailyTestsuites(project, 30)

	respond(w, http.StatusOK, testsuites)
}
