package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	rice "github.com/GeertJohan/go.rice"
	"github.com/fetzi/goverage/config"
	"github.com/fetzi/goverage/handlers"
	"github.com/fetzi/goverage/storage"
	"github.com/fetzi/goverage/worker"
	"github.com/google/logger"
	"github.com/gorilla/mux"
)

var verbose = flag.Bool("verbose", false, "print info level logs to stdout")

func main() {
	logDir := "./logs"

	if _, err := os.Stat(logDir); os.IsNotExist(err) {
		os.Mkdir(logDir, 0755)
	}

	lf, err := os.OpenFile(fmt.Sprintf("%s/goverage.log", logDir), os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0660)

	if err != nil {
		logger.Fatalf("Failed to open log file: %v", err)
	}
	defer lf.Close()

	logger := logger.Init("goverage", *verbose, false, lf)
	defer logger.Close()

	logger.Infoln("starting goverage")

	defer func() {
		if r := recover(); r != nil {
			var ok bool
			err, ok := r.(error)
			if !ok {
				logger.Fatalln(err)
			}
		}
	}()

	conf := config.Load("./config.toml")
	storage, err := storage.Connect(conf.Database.Driver, conf.Database.DSN)

	if err != nil {
		logger.Fatalln(err)
		return
	}

	appContext := handlers.AppContext{
		Storage: storage,
		Config:  conf,
	}

	var wait time.Duration
	flag.DurationVar(&wait, "graceful-timeout", time.Second*15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
	flag.Parse()

	r := mux.NewRouter().StrictSlash(true)
	r.HandleFunc("/coverage/upload", appContext.UploadCoverageHandler)
	r.HandleFunc("/testsuite/upload", appContext.UploadTestHandler)
	r.HandleFunc("/projects", appContext.GetProjectsHandler)
	r.HandleFunc("/projects/{project}", appContext.GetProjectHandler)
	r.HandleFunc("/projects/{project}/coverage", appContext.GetCoverageHandler)
	r.HandleFunc("/projects/{project}/coverage/daily", appContext.GetDailyCoverageHandler)
	r.HandleFunc("/projects/{project}/testsuites", appContext.GetTestsuiteHandler)
	r.HandleFunc("/projects/{project}/testsuites/daily", appContext.GetDailyTestsuiteHandler)
	r.PathPrefix("/").Handler(http.FileServer(rice.MustFindBox("html-files").HTTPBox()))

	srv := &http.Server{
		Handler:      r,
		Addr:         conf.Web.Host,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			logger.Fatalln(err)
		}
	}()

	worker.Aggregate(appContext.Storage, 5)

	ctx, cancel := context.WithTimeout(context.Background(), wait)

	c := make(chan os.Signal, 1)
	signal.Notify(c,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	<-c
	defer cancel()

	srv.Shutdown(ctx)
	logger.Infoln("shutting down")
	fmt.Println("shutdown handled - exiting")
	os.Exit(0)
}
