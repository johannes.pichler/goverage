package models

import (
	"fmt"
	"time"

	"github.com/google/jsonapi"
)

// Project defines the project information
type Project struct {
	ID        uint       `gorm:"primary_key" jsonapi:"primary,project"`
	Name      string     `jsonapi:"attr,name"`
	Label     string     `jsonapi:"attr,label"`
	CreatedAt time.Time  `jsonapi:"attr,created-at,iso8601"`
	UpdatedAt time.Time  `jsonapi:"attr,updated-at,iso8601"`
	DeletedAt *time.Time `sql:"index"`
}

func (project Project) JSONAPILinks() *jsonapi.Links {
	return &jsonapi.Links{
		"self":             fmt.Sprintf("/projects/%s", project.Name),
		"coverage":         fmt.Sprintf("/projects/%s/coverage", project.Name),
		"coverage-daily":   fmt.Sprintf("/projects/%s/coverage/daily", project.Name),
		"testsuites":       fmt.Sprintf("/projects/%s/testsuites", project.Name),
		"testsuites-daily": fmt.Sprintf("/projects/%s/testsuites/daily", project.Name),
	}
}
