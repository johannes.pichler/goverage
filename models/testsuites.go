package models

import "time"

// Testsuite defines the junit testsuite data
type Testsuite struct {
	ID        uint `gorm:"primary_key" jsonapi:"primary,testsuites"`
	ProjectID uint
	Errors    uint       `jsonapi:"attr,errors"`
	Failures  uint       `jsonapi:"attr,failures"`
	Skipped   uint       `jsonapi:"attr,skipped"`
	Time      float64    `jsonapi:"attr,time"`
	Tests     uint       `jsonapi:"attr,tests"`
	CreatedAt time.Time  `jsonapi:"attr,created-at,iso8601"`
	UpdatedAt time.Time  `jsonapi:"attr,updated-at,iso8601"`
	DeletedAt *time.Time `sql:"index"`
}

// TestsuiteDaily defines the junit testsuite data
type TestsuiteDaily struct {
	ID        uint `gorm:"primary_key" jsonapi:"primary,daily-testsuites"`
	ProjectID uint
	Date      time.Time  `jsonapi:"attr,date"`
	Errors    float64    `jsonapi:"attr,errors"`
	Failures  float64    `jsonapi:"attr,failures"`
	Skipped   float64    `jsonapi:"attr,skipped"`
	Time      float64    `jsonapi:"attr,time"`
	Tests     float64    `jsonapi:"attr,tests"`
	CreatedAt time.Time  `jsonapi:"attr,created-at,iso8601"`
	UpdatedAt time.Time  `jsonapi:"attr,updated-at,iso8601"`
	DeletedAt *time.Time `sql:"index"`
}
