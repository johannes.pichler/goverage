package models

import "time"

// Coverage defines the key coverage metrics
type Coverage struct {
	ID                  uint `gorm:"primary_key" jsonapi:"primary,coverage"`
	ProjectID           uint
	Statements          int        `jsonapi:"attr,statements"`
	CoveredStatements   int        `jsonapi:"attr,covered-statements"`
	TotalCoverage       float64    `jsonapi:"attr,total-coverage"`
	Conditionals        int        `jsonapi:"attr,conditionals"`
	CoveredConditionals int        `jsonapi:"attr,covered-conditionals"`
	ConditionalCoverage float64    `jsonapi:"attr,conditial-coverage"`
	Methods             int        `jsonapi:"attr,methods"`
	CoveredMethods      int        `jsonapi:"attr,covered-methods"`
	MethodCoverage      float64    `jsonapi:"attr,method-coverage"`
	Elements            int        `jsonapi:"attr,elements"`
	CoveredElements     int        `jsonapi:"attr,covered-elements"`
	ElementCoverage     float64    `jsonapi:"attr,element-coverage"`
	LinesOfCode         int        `jsonapi:"attr,lines-of-code"`
	CreatedAt           time.Time  `jsonapi:"attr,created-at,iso8601"`
	UpdatedAt           time.Time  `jsonapi:"attr,updated-at,iso8601"`
	DeletedAt           *time.Time `sql:"index"`
}

// CoverageDaily defines the daily coverage aggregate
type CoverageDaily struct {
	ID                  uint `gorm:"primary_key" jsonapi:"primary,daily-coverage"`
	ProjectID           uint
	Date                time.Time  `jsonapi:"attr,date,iso8601"`
	Coverage            float64    `jsonapi:"attr,coverage"`
	ConditionalCoverage float64    `jsonapi:"attr,conditional-coverage"`
	MethodCoverage      float64    `jsonapi:"attr,method-coverage"`
	ElementCoverage     float64    `jsonapi:"attr,element-coverage"`
	LinesOfCode         float64    `jsonapi:"attr,lines-of-code"`
	CreatedAt           time.Time  `jsonapi:"attr,created-at,iso8601"`
	UpdatedAt           time.Time  `jsonapi:"attr,updated-at,iso8601"`
	DeletedAt           *time.Time `sql:"index"`
}
